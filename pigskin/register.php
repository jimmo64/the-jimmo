<?php
require_once 'Database.class.php';
require_once 'Settings.class.php';

	$userNameErr = $fNameErr = $lNameErr = $emailErr = $passwordErr1 =  $passwordErr2 = $errAll = "";
	$db = new Database();
	
	if(strtoupper($_SERVER['REQUEST_METHOD']) == 'POST'){
		if(empty($_POST["fName"]) || $_POST["fName"] == "First name"){
			$fNameErr = "Required";
		}else{
			$fName = $_POST["fName"];
		}
		if(empty($_POST["lName"]) || $_POST["lName"] == "Last name"){
			$lNameErr = "Required";
		}else{
			$lName = $_POST["lName"];
		}
		if(empty($_POST["email"]) || $_POST["email"] == "Email"){
			$emailErr = "Required";
		}elseif($db->dupEmailCheck(strtolower($_POST["email"])) != false){
			$emailErr = "This email already exists in our system, please login with your account.";
		}else{
			$email = strtolower($_POST["email"]);
		}
		if(empty($_POST["password1"]) && empty($_POST["password2"]) || $_POST["password1"] == "Password" || $_POST["password2"] == "Confirm Password"){
			$passwordErr1 = "Required";
			$passwordErr2 = "Required";
		}
		elseif(empty($_POST["password1"]) || $_POST["password1"] == "Password"){
			$passwordErr1 = "Required";
		}elseif(empty($_POST["password2"]) || $_POST["password2"] == "Confirm Password"){
			$passwordErr2 = "Required";
		}
		elseif($_POST["password1"] != $_POST["password2"]){
			$passwordErr2 = "Passwords must match";	
			$passmatch = 1;
		}else{
			$password = $_POST["password1"];
			$passmatch = 0;
		}
		
		if($fName == "" || $lName == "" || $email == "" || $password == "" || $passmatch == 1){
			$errAll = "<h3>Wide right!</h3><p>There appears to be some errors, please correct errors marked below:</p>";
			//echo $fName . "<br />" . $lName . "<br />" . $email . "<br />" . $password . "<br />" . $passmatch;
		}else{
			$db = new Database;
			$fName = $db->esc($fName);
			$lName = $db->esc($lName);
			$email = $db->esc($email);
			$password = $db->esc($password);
			$joinDate = $db->esc(date("F j, Y, g:i a"));
			$register = $db->query("INSERT INTO `Users` VALUES ('', '', '$password', '$email', '$joinDate', '$joinDate', '$fName', '$lName', '')");
			//$db->query("");// add query
			//*echo "Data connection is a go!";
			if($register == false){
				$errAll = "Something went wrong, please try again!";
			}else{
			$matching_users = $db->query("SELECT 1 FROM Users WHERE Email='$email' AND Password='$password' LIMIT 1");
			if ($matching_users) {
			    // User exists; log user in.
				$fingerprint = md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
				$_SESSION['last_active'] = time();
				$_SESSION['fingerprint'] = $fingerprint;
			    $_SESSION['email_address'] = $email_address;
			    $errAll =  "You are now logged in.";
				header("Location: ".$settings->directory."user/");
				die();
			} else {
			    // Login failed; re-display login form.
				$errAll = "Login Failed";
			}
			}
		}
	}
		$settings = new Settings;
		include $settings->directory."header.php";
?>


    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
    	<h1>Register</h1>
        <p></p>
        </div>
    </div>
      <div class="container">
        <?php echo $errAll; ?>
        <p> <form class="" action="<?php echo $settings->directory."register.php" ?>" method="post">
        <div class="row-fluid">
            <div class="form-group span6">
              <input type="text" placeholder="First name" name="fName" value="<?php echo $_POST["fName"]; ?>" class="form-control">
              <br /><span class="error"><?php echo $fNameErr; ?></span>
            </div>
            <div class="form-group span6">
              <input type="text" placeholder="Last name" name="lName" value="<?php echo $_POST["lName"]; ?>" class="form-control">
              <br /><span class="error"><?php echo $lNameErr; ?></span>
            </div>
           </div>
        <div class="row-fluid">
            <div class="form-group span6">
              <input type="text" placeholder="Email" name="email" value="<?php echo $_POST["email"]; ?>" class="form-control">
              <br /><span class="error"><?php echo $emailErr; ?></span>
            </div>
            <div class="form-group span6">
              <input type="password" placeholder="Password" name="password1" value="<?php echo $_POST["password1"]; ?>" class="form-control">
              <br /><span class="error"><?php echo $passwordErr1; ?></span>
            </div>
         </div>
         <div class="row-fluid">
         <div class="form-group span6 hidden-phone"><button type="submit" class="btn btn-primary btn-lg">Register</button></div>
            <div class="form-group span6">
              <input type="password" placeholder="Confirm Password" name="password2" value="<?php echo $_POST["password2"]; ?>" class="form-control">
              <br /><span class="error"><?php echo $passwordErr2; ?></span>
            </div>
         </div>
            <button type="submit" class="btn btn-primary btn-lg hidden-desktop hidden-tablet">Register</button>
          </form></p>
          


<?php 	include $settings->directory."footer.php"; ?>
