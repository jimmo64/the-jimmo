<?php
require_once 'Database.class.php';
$db = new Database();

class Groups{

  public function getUserGroup($userID){
	  $db = new Database();
	 if (mysqli_connect_errno($db->connect()))
  	{
  		echo "Failed to connect to MySQL: " . mysqli_connect_error();
  	}
	
	$sqlQuery = "SELECT g.Name FROM UserGroup ug JOIN `Group` g ON ug.GroupID = g.GroupID WHERE UserID = '$userID'";
    // Send a query to the database
	$result = mysqli_query($db->connect(),$sqlQuery);
	if($result == false){
		return false;
	}else{
		$i = 0;
	while($row = mysqli_fetch_array($result))
  {
	  return $row['Name'];
	  $i++;
  }
	}
  }
  
    public function getUserGroupID($userID){
	  $db = new Database();
	 if (mysqli_connect_errno($db->connect()))
  	{
  		echo "Failed to connect to MySQL: " . mysqli_connect_error();
  	}
	
	$sqlQuery = "SELECT g.GroupID FROM UserGroup ug JOIN `Group` g ON ug.GroupID = g.GroupID WHERE UserID = '$userID'";
    // Send a query to the database
	$result = mysqli_query($db->connect(),$sqlQuery);
	if($result == false){
		return false;
	}else{
		$i = 0;
	while($row = mysqli_fetch_array($result))
  {
	  return $row['GroupID'];
	  $i++;
  }
	}
  }
  
  
    public function getAllUserGroup($userID){
	  $db = new Database();
	 if (mysqli_connect_errno($db->connect()))
  	{
  		echo "Failed to connect to MySQL: " . mysqli_connect_error();
  	}
	
	$sqlQuery = "SELECT g.Name FROM UserGroup ug JOIN `Group` g ON ug.GroupID = g.GroupID WHERE UserID = '$userID'";
    // Send a query to the database
	$result = mysqli_query($db->connect(),$sqlQuery);
	if($result == false){
		return false;
	}else{
		$i = 0;
		$groupname = array();
	while($row = mysqli_fetch_array($result))
  {
	  $groupname[''.$i.''] = $row['Name'];
	  $i++;
  }
  		return $groupname;
	}
  }
  
    public function getAllUserGroupID($userID){
	  $db = new Database();
	 if (mysqli_connect_errno($db->connect()))
  	{
  		echo "Failed to connect to MySQL: " . mysqli_connect_error();
  	}
	
	$sqlQuery = "SELECT g.GroupID FROM UserGroup ug JOIN `Group` g ON ug.GroupID = g.GroupID WHERE UserID = '$userID'";
    // Send a query to the database
	$result = mysqli_query($db->connect(),$sqlQuery);
	if($result == false){
		return false;
	}else{
		$i = 0;
		$groupid = array();
	while($row = mysqli_fetch_array($result))
  {
	  $groupid[''.$i.''] = $row['GroupID'];
	  $i++;
  }
  	return $groupid;
	}
  }

	public function checkGroupLimit($groupid){
			$result = $db->query("SELECT UserLimit, NumberOfUsers FROM `Group` WHERE GroupID = '$groupid' LIMIT 1");
			$row = mysqli_fetch_array($result);
			$grouplimit = $row['UserLimit'];
			$groupUsers = $row['NumberOfUsers'];
			
			if($groupUsers < $grouplimit){
				return 1;	
			}else{
				return 0;	
			}
	}
	
	public function userGroupsNumber($userid){
			$result = $db->query("SELECT COUNT(UserID) AS countgroups FROM `UserGroup` WHERE UserID = '$userid'");
			$row = mysqli_fetch_array($result);
			if($row['countgroups'] < 3)
				return 0;
			else
				return 1;
	}
  
}
?>