<?php
require_once 'Settings.class.php';

	include $settings->directory."header.php";
?>



    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1 class="hidden-tablet">Welcome to PigSkin!</h1>
        <p>PigSkin is a NFL<sup>&reg;</sup> pick-em styled inspired game, with a twist! </p>
        <p><a href="register.php" class="btn btn-primary btn-lg">Start Now &raquo;</a></p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-lg-4">
          <h2>Groups</h2>
          <p>Compete against your friends, classmates, that one weird guy you met at the bar, or anyone! With specialized groups, customizable scoring and more, your office pool takes new meaning!</p>
          <p><a class="btn btn-default" href="register.php">Sign up Today!</a></p>
        </div>
        <div class="col-lg-4">
          <h2>The Twist</h2>
          <p>Regular pick 'em styled games allow easy recovery, you pick 1 game wrong you can make it up. PigSkin raises the bar, you have one team pick per week, if you're wrong you loose all points for that whole week! As well you can't choose the same team again in the ENTIRE SEASON. This game will force you out of your comfort zone and get to know all teams!</p>
          <p><a class="btn btn-default" href="register.php">Step up to the challenge &raquo;</a></p>
       </div>
        <div class="col-lg-4">
          <h2>Access Anywhere</h2>
          <p>Fun times will be had from anywhere, anytime. Our webiste is available and resposive to all your devices and computers</p>
          <p><a href="register.php" class="btn btn-default" href="#">Start now &raquo;</a></p>
        </div>
      </div>

<?php 	include $settings->directory."footer.php"; ?>
