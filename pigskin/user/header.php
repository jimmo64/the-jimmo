<?php 
include "../LoginCheck.php"; 
require_once '../Database.class.php';
require_once '../Groups.class.php';

	$db = new Database();
  $firstName = $db->getUserFirstName(strtolower($_SESSION['email_address']));
  $lastName = $db->getUserLastName(strtolower($_SESSION['email_address']));
    $userID = $db->getUserID(strtolower($_SESSION['email_address']));
	$groups = new Groups();
	
	$groupName = $groups->getAllUserGroup($userID);
	$groupID = $groups->getAllUserGroupID($userID);
	$active = "class=\"active\"";
	//$usersRecord = $db->getUsersRecord($userID);

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="ico/favicon.png">

    <?php echo "<title>PigSkin</title>"; ?>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap2.css" rel="stylesheet">
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
	<link href="../css/pigskin.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../jumbotron.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../user/">PigSkin</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="../user/">Home</a></li>
            <li><a href="pick.php">Pick</a></li>
            <li class="dropdown">
              <a href="groups.php" class="dropdown-toggle" data-toggle="dropdown">Groups <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="create-a-group.php">Create a Group</a></li>
                <li><a href="search-join-group.php">Join a Group</a></li>
                <?php if($groupName != false){
					for($i = 0; $i < sizeof($groupName); $i++){ 
				echo "<li class=\"divider\"></li>";
			    echo "<li class=\"dropdown-header\">".$groupName[''.$i.'']."</li>";
                echo "<li><a href=\"standings.php?group=".$groupID[''.$i.'']."\">Standings</a></li>";
                echo "<li><a href=\"standings.php?group=".$groupID[''.$i.'']."\">Members</a></li>";
					}
				}
				
				?>

              </ul>
            </li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
          <div class="navbar-form navbar-right" style="color: #fff;">
          <?php echo "Welcome ". $firstName . " " . $lastName; ?>
            </div>
            </div>
        </div><!--/.navbar-collapse -->
      </div>
    </div>