<?php
		include "header.php";
		
		
		$groupNameErr = $numUsersErr = $regSeasonPtsErr = $superbowlPtsErr = $playoffPtsErr =  "";
	
	if(strtoupper($_SERVER['REQUEST_METHOD']) == 'POST'){
		if(empty($_POST["groupName"])){
			$groupNameErr = "Required";
		}else{
			$groupNameq = $_POST["groupName"];
		}
		if(empty($_POST["numMembers"]) || $_POST["numMembers"] == 0){
			$numUsersErr = "Required";
		}else{
			$numerUsers = $_POST["numMembers"];
		}
		if(empty($_POST["regSeasonPoints"]) || $_POST["regSeasonPoints"] == 0){
			$regSeasonPtsErr = "Required";
		}else{
			$regSeasonPts = $_POST["regSeasonPoints"];
		}
		if(empty($_POST["playoffPoints"]) || $_POST["playoffPoints"] == 0){
			$playoffPtsErr  = "Required";
		}else{
			$playoffPts = $_POST["playoffPoints"];
		}
		if(empty($_POST["superbowlPoints"]) || $_POST["superbowlPoints"] == 0){
			$superbowlPtsErr  = "Required";
		}else{
			$superbowlPts = $_POST["superbowlPoints"];
		}
		
		if($groupName == "" || $numerUsers == "" || $regSeasonPts == "" || $playoffPts == "" || $superbowlPts == ""){
			$errAll = "<h3>Wide right!</h3><p>There appears to be some errors, please correct errors marked below:</p>";
			//echo $fName . "<br />" . $lName . "<br />" . $email . "<br />" . $password . "<br />" . $passmatch;
		}else{
			//$db = new Database;
			$groupNameq = $db->esc($groupNameq);
			$numerUsers = $db->esc($numerUsers);
			$regSeasonPts = $db->esc($regSeasonPts);
			$playoffPts = $db->esc($playoffPts);
			$superbowlPts = $db->esc($superbowlPts);
			$con = mysqli_connect("localhost","ringo64_pigskin","nintendo64","ringo64_pigskin");
			$createGroup = "INSERT INTO `Group` (Name, Owner, NumberOfUsers, RegularSeason, Playoff, Superbowl, UserLimit) VALUES ('$groupNameq', '$userID', 1, '$regSeasonPts', '$playoffPts', '$superbowlPts', '$numerUsers')";
			mysqli_query($con,$createGroup);
			//$getGroupID = $db->query("SELECT GroupID FROM `Group` WHERE Name = '$groupNameq' AND Owner = '$userID' LIMIT 1");
			//$getGroupID = $db->query("SELECT LAST_INSERT_ID() AS insert_id");
			$getGroupID = mysqli_insert_id($con); //$mysqli->insert_id; //
			//$rowgroup = mysqli_fetch_array($getGroupID);
			//$groupIDq = $rowgroup['insert_id'];
			echo $groupIDq . $getGroupID;
			$addUserToGroup = $db->query("INSERT INTO `UserGroup` VALUES ('', '$getGroupID', '$userID', 0, 0, 0)");
			//$db->query("");// add query
			//*echo "Data connection is a go!";
			if($createGroup == false || $getGroupID == false){
				$errAll = "Something went wrong, please try again!";
			}else{
				$success = true;
			}
		}
	}
		
?>


<?php if($success != true) { ?>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Create a Group</h1>
        <p>Hillarious and risque names are highly encouraged</p>       
      </div>
    </div>
<div class="container">
        <?php echo $errAll; ?>
        <p> <form class="" action="" method="post">
        <div class="row-fluid">
            <div class="form-group span6">
            <label>Group Name</label>
              <input type="text" name="groupName" maxlength="20" value="<?php echo $_POST["groupName"]; ?>" class="form-control">
              <br /><span class="error"><?php echo $groupNameErr; ?></span>
            </div>
            <div class="form-group span6">
            <label>Number of users limit</label>
            <select name="numMembers" class="form-control">
            	 <option value="0">--Select--</option>
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
            </select>
            </div>
           </div>
        <div class="row-fluid">
            <div class="form-group span6">
            <label>Regular Season Points</label>
            <select name="regSeasonPoints" class="form-control">
            	 <option value="0">--Select--</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
            <br />Points are awarded when a user gets that week's pick correct.
            </div>
            <div class="form-group span6">
            <label>Playoff Points</label>
            <select name="playoffPoints" class="form-control">
            	 <option value="0">--Select--</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
            <br />Points are awarded when a user gets that week's pick correct.
            </div>
         </div>
         <div class="row-fluid">
         <div class="form-group span6 hidden-phone"><button type="submit" class="btn btn-primary btn-lg">Create Group</button></div>
            <div class="form-group span6">
             <label>Superbowl Points</label>
            <select name="superbowlPoints" class="form-control">
            	 <option value="0">--Select--</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
            <br />Points are awarded when a user gets that week's pick correct.
            </div>
         </div>
            <button type="submit" class="btn btn-primary btn-lg hidden-desktop hidden-tablet">Create Group</button>
          </form></p>
	<?php 
	}elseif ($groups->userGroupsNumber($userID) == 1){
		?>
            <div class="jumbotron">
      <div class="container">
        <h1>Sorry!</h1>
        <p>You currently are a member of too many groups. Please leave on in order to create one.</p>       
      </div>
    </div>
<div class="container">
	<? }else{ ?>
        <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Success!</h1>
        <p>Your Group has been created! </p>       
      </div>
    </div>
<div class="container">
    <?php } ?>
    

<?php 	include "footer.php"; ?>
