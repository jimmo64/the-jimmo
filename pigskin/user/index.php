<?php
		include "header.php";
		$week = $db->getThisWeek();	
		$usersPick = $db->getUsersGame($userID, $week);
?>



    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Hello, <?php echo $firstName; ?></h1>
        <?php if($groupName == false){ ?>
        <p>Seems you do not belong to a group, this game is no fun to play by yourself! Let's find you a group!</p>
        <p>You can either <a class="btn btn-primary btn-lg">create your own group</a> or <a class="btn btn-primary btn-lg">join a group</a>
        <?php }else{?>
        <p>Let's do this!</p>
        <?php } ?>
      </div>
    </div>
        <?php if($groupName == false){ 
        	include "find-a-group.php";
                }else{?>
	<div class="container">
      <!-- Example row of columns -->
      <div class="row">
		<div class="span9">
          	<div class="row">
            	<div class="span6">
                <?php 
					if($week != ""){
						echo "<h3> Week " . $week . "</h3>";
						if($usersPick != ""){
							echo "<p>Your pick for this week: <strong>" . $usersPick . "</strong></p>";
						}else{
							echo "<p>You do not have a pick for this week! Let's change that! <a class=\"btn btn-primary\" href=\"pick.php\">Click Here</a></p>";	
						}
					}
					
				?>
                </div>
                <div class="span3">
                	<div>
                 <?php
				$group = new Groups();
				$usergroup = $group->getAllUserGroup($userID);
				$groupallid = $group->getAllUserGroupID($userID);
				echo "<h3>Your Group(s)</h3>";
				if($usergroup != false){
					for($i = 0; $i < sizeof($usergroup); $i++){

					echo "<strong><a href=\"standings.php?group=". $groupallid[''.$i.''] . "\">" . $usergroup[''.$i.''] . "</a></strong>";	
					echo "<p>Record: " . $db->getUsersRecord($userID, $groupallid[''.$i.'']) . "</p>";
					}
				}
				?>
                    </div>
                    <div>&nbsp;</div>
                </div>
             </div>
          </div>
       </div>
               <?php } ?>


<?php 	include "footer.php"; ?>
