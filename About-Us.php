<?php include 'header.php' ?>

<section class="jimmo-bg about-us">
    <div class="row">
        <div class="eight columns centered">
            <div class="headline">
                <h1>About Us</h1>
                <p>All your Digital needs, taken care of.</p>
            </div>
        </div>
    </div>
</section>

<section class="about-us">
	<div class="row">
		<div class="seven columns content">
        	<h2>Obessed with Digital Technology</h2>
        </div>
    </div>
  	<div class="about-us-desc">
    	<div class="row">
        	<div class="seven columns content">
            <p>
Great Digital Experiences lead to happy visitors and happy visitors lead to more customers. We believe your development, design and digital marketing have to come together as a coexisting life-form to best utilize your online engagement, while at the same time living in a seamless web and mobile world.</p><p>
We are what you would call obsessed with digital technology and have been for nearly the past two decades. It’s our passion, our entertainment and most importantly, our livelihood! Located in beautiful Naples, FL USA, we have been creating just as beautiful Digital Experiences since 1997. </p><p>
We love to work with any technology but we have a special soft spot for <a style="color: #00aeef;" href="https://kentico.com" target="_blank">Kentico CMS</a> and the <a style="color: #00aeef;" href="https://www.invisionpower.com/clients/index.php?app=nexus&module=promotion&section=referral&id=77166&direct=aHR0cHM6Ly93d3cuaW52aXNpb25wb3dlci5jb20v" target="_blank">IPS Community Suite</a>.</p>
		<p></p>
        </div>
        <div class="five columns content">
        <ul>
        <li><img src="img/jimmo.png" gumby-retina /></li>
        <li>Jim Morrissey</li>
        <li>CEO / President</li>
        </ul>
        </div>
    </div>
</section>

<?php include 'footer.php' ?>