<?php include 'header.php'; 

?>

<section class="jimmo-bg animation">
    <div class="row">
        <div class="eight columns centered">
            <div class="firstline">
                <h1>Has your company’s <span class="fairytale">Fairytale</span></h1>
            </div>
            <div class="secondline">
            	<h1 class="break-space">Digital Experience</h1>
            </div>
            <div class="thirdline">
            	<h1 class="break-space">turned into a <span class="nightmare">Nightmare?</span></h1>
            </div>
	        <p>We can tame your dragons! We specialize in digital experience, from websites to applications. <a href="/our-services">Read more on our services</a>.</p>
        </div>
    </div>
</section>

<section class="development service-desc">
	<div class="row">
    	<div class="twelve columns centered">
        	<div class="three columns">
                <div class="title">
                	<img src="/img/code.png" />
                    <h2>DEVELOPMENT</h2>
                </div>
            </div>
            <div class="eight columns">
                <div class="desc">
                    <h3>We speak Geek... and English!</h3>
                    <p>We specialize in developing web applications in multiple languages as well in multiple frameworks. We utilize our knowledge to make your website function the way you want it to and in ways you may not even thought of.</p>
                    <button class="cta-btn contact"onclick="location.href='/contact-us'">Contact Us</button><button class="cta-btn learn-more" onclick="location.href='Our-Services.php'">Learn More</button>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="design service-desc">
	<div class="row">
    	<div class="twelve columns centered">
        	<div class="three columns">
                <div class="title">
                <img src="/img/design.png" />
                    <h2>design</h2>
                </div>
            </div>
            <div class="eight columns">
                <div class="desc">
                    <h3>Get the Glamour!</h3>
                    <p>We can help you build a design for your website that won’t just make you proud but make your customers excited to be using your website and ready to do business with you! We specialize in design that will give your users the best experience moving throughout your website.</p>
                    <button class="cta-btn contact"onclick="location.href='/contact-us'">Contact Us</button><button class="cta-btn learn-more" onclick="location.href='Our-Services.php'">Learn More</button>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="digi-mark service-desc">
	<div class="row">
    	<div class="twelve columns centered">
        	 <div class="three columns">
                <div class="title">
                <img src="/img/digi-mark.png" />
                    <h2>digital marketing</h2>
                </div>
            </div>
            <div class="eight columns">
                <div class="desc">
                    <h3>To the massses!</h3>
                    <p>Our Digital Marketing techniques are proven to attract the best customers for you. From Google AdWords to our Social Media strategies you can’t go wrong with attracting new visitors to your website.</p>
                    <button class="cta-btn contact"onclick="location.href='/contact-us'">Contact Us</button><button class="cta-btn learn-more" onclick="location.href='Our-Services.php'">Learn More</button>
                </div>
            </div>
        </div>
    </div>
</section>

<?php

//custom.setJS("/js/custom/home-animation.js");

?>

<?php include 'footer.php' ?>