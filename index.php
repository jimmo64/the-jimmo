<?php include 'header.php'; 

?>

<section class="jimmo-bg animation">
    <div class="row">
        <div class="eight columns centered">
            <div class="firstline">
                <h1>Has your company’s <span class="fairytale">Fairytale</span></h1>
            </div>
            <div class="secondline">
            	<h1 class="break-space">Digital Experience</h1>
            </div>
            <div class="thirdline">
            	<h1 class="break-space">turned into a <span class="nightmare">Nightmare?</span></h1>
            </div>
	        <p>We can tame your dragons! We specialize in digital experience, from websites to applications. <a href="/our-services">Read more on our services</a>.</p>
        </div>
    </div>
</section>

<section class="development service-desc">
	<div class="row">
    	<div class="twelve columns centered">
        	<div class="seven columns">
                <div class="title">
                	<img src="/img/responsive-screens.png" />
                </div>
            </div>
            <div class="four columns">
                <div class="desc">
                    <h3>Your Webiste. Their Devices.</h3>
                    <p>Users are no longer locked to their desktops, why should your website? Our responsive design will be able to put your website at the finger tips of your customers no matter the device they're using.</p>
                    <button class="cta-btn contact"onclick="location.href='/contact-us'">Contact Us</button><button class="cta-btn learn-more" onclick="location.href='/our-services'">Learn More</button>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="design service-desc">
	<div class="row">
    	<div class="twelve columns centered">
        	<div class="three columns">
                <div class="title">
                    <h2><a href="/our-services">What We Do</a></h2>
                </div>
            </div>
            <div class="eight columns">
                <div class="desc">
					<div>
						<h3><a href="/our-services">Development</a></h3>
						<ul>
							<li>Responsive Websites</li>
							<li>Content Management, Social/Community and Intranet Solutions</li>
							<li>Ecommerce and Web Applications</li>
							<li>Consulting</li>
						</ul>
					</div>
					<div>
					<h3><a href="/our-services">Design</a></h3>
						<ul>
							<li>Responsive Design</li>
							<li>Digital Design</li>
							<li>Consulting</li>
						</ul>
					</div>
					<div>
						<h3><a href="/our-services">Digital Marketing</a></h3>
						<ul>
							<li>Search engine optimization</li>
							<li>Conversion rate optimization</li>
							<li>Lead generation</li>
							<li>Marketing automation solutions</li>
							<li>Content marketing programs</li>
							<li>Social media strategy</li>
							<li>Paid search and display</li>
						</ul>
					</div>
					<button class="cta-btn learn-more right-align" onclick="location.href='/our-services'">Learn More</button>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="digi-mark service-desc contact-us-home">
	<div class="row">
    	<div class="eight columns centered">
                <div class="title">
                    <h2>Let's Conquer these Dragons together.<br /><a href="/contact-us">Contact us, today!</a></h2>
                </div>
        </div>
    </div>
</section>

<?php

//custom.setJS("/js/custom/home-animation.js");

?>

<?php include 'footer.php' ?>