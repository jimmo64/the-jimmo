<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en" itemscope="" itemtype="http://schema.org/Product"> <!--<![endif]-->
<!--[if !IE]><html lang="en" xmlns="http://www.w3.org/1999/xhtml" ><![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">

<?php 

//callClass();

class open {

public $bodyclass = "";
public $title = "";

	
	function callClass() {
		$value = $_SERVER['REQUEST_URI'];
		
		if($value == "homepage" || strpos($value,'index.php') !== false){
			$this->title = "We Build Digital Experiences";
			$this->bodyclass = "homepage";
		}
		elseif($value == "services" || strpos($value,'services') !== false){
			$this->title = "Services";
			$this->bodyclass = "services";
		}
		elseif($value == "about-us" || strpos($value,'about-us') !== false){
			$this->title = "About Us";
			$this->bodyclass = "about-us";
		}
		elseif($value == "contact" || strpos($value,'contact-us') !== false){
			$this->title = "Contact Us";
			$this->bodyclass = "contact";
		}
		else {
			$this->title = "We Build Digital Experiences";
			$this->bodyclass = "homepage";
		}
		
		echo "<title>" . $this->title ." - The Jimmo</title>";
		echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/site.css?site=1\">";
		echo "<script src=\"/js/libs/modernizr-2.6.2.min.js\"></script>";
		echo "</head>";
		echo "<body class=\"thejimmo ".$this->bodyclass ."\">";	
	}
	

}

$o = new open();
$o->callClass();



?>


<header>
	<div class="navbar row">
            <div class="logo">
                <!-- i class="icon-thejimmo-logo-header custom" data-icon="/e600"></i> -->
                <a href="/"><img src="/img/logo.png" /></a>
            </div>
            <ul class="navigation">
            	
                <li><a class="jnav nav-1" href="/our-services">Services</a></li>
                <li><a class="jnav nav-2" href="/about-us">About</a></li>
                <!-- li><a class="jnav" href="">Clients</a></li -->
                <li><a class="jnav contact-us nav-3" href="/contact-us">Contact Us</a></li>
            </ul>
            <a class="head-btn desktop" href="/contact-us">
            	<div class="contact-btn">
                	Contact Us
                 </div>
            </a>
            <a class="mobile-menu mobile toggle" gumby-trigger=".navigation"><img src="/img/mobile-nav.png" border="0" /></a>
    </div>
</header>
