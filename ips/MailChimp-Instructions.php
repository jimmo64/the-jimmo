
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

<link rel="shortcut icon" href="https://thejimmo.com/favicon.ico" type="image/x-icon">
<link rel="icon" href="https://thejimmo.com/favicon.ico" type="image/x-icon">

<title>We Build Digital Experiences - The Jimmo</title><link rel="stylesheet" type="text/css" href="https://thejimmo.com/css/site.css?=1"><script src="https://thejimmo.com/js/libs/modernizr-2.6.2.min.js"></script></head><body class="thejimmo kentico-prod homepage">

<header>
	<div class="navbar row">
            <div class="logo">
                <!-- i class="icon-thejimmo-logo-header custom" data-icon="/e600"></i> -->
                <a href="https://thejimmo.com"><img src="https://thejimmo.com/img/logo.png" /></a>
            </div>
            <ul class="navigation dropdown">
            	
                <li><a class="jnav nav-1" href="https://thejimmo.com/our-services">Services</a></li>
                <li><a class="jnav nav-2" href="https://thejimmo.com/about-us">About</a></li>
                <!-- li><a class="jnav" href="">Clients</a></li -->
                <li><a class="jnav contact-us nav-3" href="https://thejimmo.com/contact-us">Contact Us</a></li>
            </ul>
            <a class="head-btn desktop" href="https://thejimmo.com/contact-us">
            	<div class="contact-btn">
                	Contact Us
                 </div>
            </a>
            <a class="mobile-menu mobile toggle" gumby-trigger=".navigation"><img src="https://thejimmo.com/img/mobile-nav.png" border="0" /></a>
    </div>
</header>



<section class="jimmo-bg about-us">
    <div class="row">
        <div class="eight columns centered headline">
			<h1>[JIMMO] MailChimp Sync</h1>
            <p>Instructions for Install on IPS 4</p>
		</div>
	</div>
</section>


<section class="product-description" style="background:#f1f1f1; padding-top: 40px;padding-bottom: 40px;color: #424242;">        
	<div class="row">
    	<div class="twelve columns">

<p>These instructions will walk you through setting up the [JIMMO] MailChimp Sync plugin for IPS.</p>

<h3 style="color: #424242;">Step 1</h3>
<p>Unzip Package to your local machine.</p>
<h3 style="color: #424242;">Step 2</h3>
<p>Upload "mailchimp-api" folder to the root of your server. Do NOT change the structure of this folder.</p>
<p>Remember this location as you'll need it later.</p>
<h3 style="color: #424242;">Step 3</h3>
<p>Go to your IPS instance. Install the plugin to your instance by going to your ACP -> Plugins -> Install Plugin. Upload the XML in provided area.
<h3 style="color: #424242;">Step 4</h3>
Go to your MailChimp account and get the following, keep these handy/write them down as you'll need them in the next step:
<ul>
        <li>MailChimp API Key (To get it, go <a style="color:#118ac9;" href="https://us2.admin.mailchimp.com/account/api/" target="_blank">here</a>)</li>
	<li>MailChimp List ID (NOT name, ID)</li>
<ul><li>To get to this go to: Lists -> Settings -> List name &amp; defaults</li></ul>
<li>MailChimp <strong>TAGNAME</strong> for the Merge Field for Member's Name (if you do not already have this in your list, you will need to create it.)
<ul><li>To get to this go to: Lists -> Settings -> List fields and *|MERGE|* tags</li></ul></li>
            	
    </ul>

<h3 style="color: #424242;">Step 5</h3>
<p>On successful install, click the "Edit"/pencil button. </p>
<ul>
	<li>In the "API Info" header, please insert the information that you collected in steps 4 and 2.<p>For "MailChimp API Path" you can use the suggested path if that is correct.</p></li>
    <li>In the "List Info" header, please insert the information you collected in the later half of step 4.</li>
    </ul>


   <p><strong>That's it!</strong> The plugin is installed and ready for use!</p>
   <h3 style="color: #424242;">Need to be able to sync all your profile fields? <a style="color:#118ac9;" href="http://community.invisionpower.com/files/file/7484-jimmo-mailchimp-sync-pro/" target="_blank">Purchase the PRO version</a>, only $10!</h3>
    <h3 style="color: #424242;">Have any questions? We're here to help!</h3>
		</div>
	</div>
</section>

<footer>
	<div class="row">
    	<div class="twelve columns centered desktop">
            <div class="copyright">
            	<div class="four pull_right">
            		<a href="/"><img src="https://thejimmo.com/img/logo-footer.png" /></a>
                </div>
                <div class="clearfix">&nbsp;</div>
                <a href="https://thejimmo.com/Contact-Us.php"><span>Contact Us</span></a>&copy; 2014 The Jimmo
            </div>
        </div>
        <div class="five columns centered phones">
            <div class="copyright footer-content">
                <div class="four centered">
            		<a href="/"><img src="https://thejimmo.com/img/logo-footer.png" /></a>
                    <span><a href="https://thejimmo.com/Contact-Us.php">Contact Us</a></span>&copy; 2014 The Jimmo
                </div>
                
            </div>
        </div>
        
    </div>
</footer>

<script src="https://thejimmo.com/js/vendor/jquery.js"></script>
<script gumby-touch="https://thejimmo.com/js/libs" src="https://thejimmo.com/js/libs/gumby.min.js"></script>


<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(66569853); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/66569853ns.gif" /></p></noscript>

</body>
</html>