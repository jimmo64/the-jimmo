function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if( !emailReg.test( $email ) ) {
    return false;
  } else {
    return true;
  }
}

$('input[name="email"]').change(function() {
		if(!validateEmail($(this).val())){
			$(this).addClass("highlight");
            isFormValid = false;
			$('form #validemail').css('display', 'block');
		}else{
			$(this).removeClass("highlight");
			$(this).addClass("valid");
			$('form #validemail').css('display', 'none');
		}
	});

$("form").submit(function(){
    var isFormValid = true;
	
	
	
	$('input').change(function() { 
		if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        }
        else{
            $(this).removeClass("highlight");
			$(this).addClass("valid");
        }
	});
	
	$('textarea').change(function() { 
		if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        }
        else{
            $(this).removeClass("highlight");
			$(this).addClass("valid");
        }
	});

    $("input").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        }
        else{
            $(this).removeClass("highlight");
			$(this).addClass("valid");
        }
    });
	
	$("textarea").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        }
        else{
            $(this).removeClass("highlight");
			$(this).addClass("valid");
        }
    });

    if (!isFormValid) $('form #validationerror').css('display', 'block');

    return isFormValid;
})