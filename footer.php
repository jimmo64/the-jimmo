<footer>
	<div class="row">
    	<div class="twelve columns centered desktop">
            <div class="copyright">
            	<div class="four pull_right">
            		<a href="/"><img src="/img/logo-footer.png" /></a>
                </div>
                <div class="clearfix">&nbsp;</div>
                <a href="Contact-Us.php"><span>Contact Us</span></a>&copy; 2015 The Jimmo
            </div>
        </div>
        <div class="five columns centered phones">
            <div class="copyright footer-content">
                <div class="four centered">
            		<a href="/"><img src="/img/logo-footer.png" /></a>
                    <span><a href="/contact-us">Contact Us</a></span>&copy; 2015 The Jimmo
                </div>
                
            </div>
        </div>
        
    </div>
</footer>

<script src="/js/vendor/jquery.js"></script>
<script gumby-touch="/js/libs" src="/js/libs/gumby.js"></script>
<script src="/js/libs/ui/gumby.retina.js"></script>
<script src="/js/libs/ui/gumby.fixed.js"></script>
<script src="/js/libs/ui/gumby.skiplink.js"></script>
<script src="/js/libs/ui/gumby.toggleswitch.js"></script>
<script src="/js/libs/ui/gumby.checkbox.js"></script>
<script src="/js/libs/ui/gumby.radiobtn.js"></script>
<script src="/js/libs/ui/gumby.tabs.js"></script>
<script src="/js/libs/ui/gumby.navbar.js"></script>
<script src="/js/libs/ui/gumby.fittext.js"></script>
<script src="/js/libs/ui/jquery.validation.js"></script>
<script src="/js/libs/gumby.init.js"></script>


<script src="/js/home-animation.js"></script>
<script src="/js/form-validation.js"></script>

<script>

</script>

<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(66569853); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/66569853ns.gif" /></p></noscript>

</body>
</html>
