<?php include 'header.php' ?>
<div class="our-services">

<section class="jimmo-bg">
    <div class="row">
        <div class="eight columns centered">
            <div class="headline">
                <h1>Serivces</h1>
                <p>What can we help you with?</p>
            </div>
          </div>
   </div>
</section>

<section class="development service-desc">
	<div class="row">
    	<div class="twelve columns centered">
        	<div class="three columns">
                <div class="title">
                	<i class="code"></i>
                    <h2>DEVELOPMENT</h2>
                </div>
            </div>
            <div class="eight columns">
                <div class="desc">
                    <h3>We speak Geek... and English!</h3>
                    <p>We specialize in developing web applications in multiple languages as well in multiple frameworks. We utilize our knowledge to make your website function the way you want it to and in ways you may not even thought of.</p>
                    <p>Solving your business' challenges with technology solutions is what we do best. We love creating interesting, useable web and mobile projects.
                    </p>
                    <ul>
                    	<li>Responsive Websites</li>
						<li>Content Management, Social/Community and Intranet Solutions</li>
						<li>Ecommerce and Web Applications</li>
						<li>Consulting</li>
                     </ul>
                    <button class="cta-btn contact" onclick="location.href='Contact-Us.php'">Contact Us</button>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="design service-desc">
	<div class="row">
    	<div class="twelve columns centered">
        	<div class="three columns">
                <div class="title">
                <i class="design"></i>
                    <h2>design</h2>
                </div>
            </div>
            <div class="eight columns">
                <div class="desc">
                    <h3>Get the Glamour!</h3>
                    <p>We can help you build a design for your website that won’t just make you proud but make your customers excited to be using your website and ready to do business with you! We specialize in design that will give your users the best experience moving throughout your website.</p>
                    <ul>
                    	<li>Responsive Design</li>
						<li>Digital Design</li>
						<li>Consulting</li>
                    </ul>
                    <button class="cta-btn contact" onclick="location.href='Contact-Us.php'">Contact Us</button>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="digi-mark service-desc">
	<div class="row">
    	<div class="twelve columns centered">
        	 <div class="three columns">
                <div class="title">
                <i class="digi-mark"></i>
                    <h2>digital marketing</h2>
                </div>
            </div>
            <div width="100" class="eight columns">
                <div class="desc">
                    <h3>To the massses!</h3>
                    <p>Our Digital Marketing techniques are proven to attract the best customers for you. From Google AdWords to our Social Media strategies you can’t go wrong with attracting new visitors to your website.</p>
                    <ul>
                    	<li>Search engine optimization</li>
						<li>Conversion rate optimization</li>
						<li>Lead generation</li>
						<li>Marketing automation solutions</li>
						<li>Content marketing programs</li>
						<li>Social media strategy</li>
						<li>Paid search and display</li>
                    </ul>                        
                    <button class="cta-btn contact" onclick="location.href='Contact-Us.php'">Contact Us</button>
                </div>
            </div>
        </div>
    </div>
</section>

</div>



<?php include 'footer.php' ?>