﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>The Jimmo - Contact - Web Design, Development and more!</title>

<link href="css.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="nav"><a href="/">Home</a><a href="port.php">Portfolio</a><a href="whatwedo.php">What We Do</a><a class="end" href="contact.php" style="border-right: 1px solid #555;">Contact</a></div>
<div class="container" style="width:960px;margin-left:auto;margin-right:auto;">
<div class="mid"><div style="margin-left:550px;width:400px;height:100px;"><br /><p class="title">Together, we can make the web a better place</p></div></div>
<!--<div class="curvebox" style="width:200px;"><h1 style="margin-top:-6px; color:#fff;margin-bottom:0px;">Nav</h1><a href="port.php">Portfolio</a><br /><a href="about.php">About Us</a><br /><a href="contact.php">Contact</a></div>-->

<div class="main" style="height: 400px;"><h1>Contact Us</h1>
<div style="margin-bottom: 20px;">Please feel free to contact us with any questions you have about our services or for a free quote!</div>
<div style="float: right;"><h3 style="margin-top: 0;">Connect with Us</h3>You can also contact us on social too!<p><a href="https://www.facebook.com/pages/The-Jimmo/239979502686372" target="_blank"><img src="/images/facebook.png" style="margin-right: 15px; border:0;" /></a><a href="https://twitter.com/?lang=en&logged_out=1#!/the_jimmo" target="_blank"><img src="/images/twitter.png" style="margin-right: 0px; border:0;" /></a></p><p>Twitter: @the_jimmo</p></div>
<div>
<?php $name = $_POST['name'];

$email = $_POST['email'];

$message = $_POST['comments2'];

$formcontent="From: $name \n Message: $message";

$recipient = "jimmo@thejimmo.com";

$subject = "Contact Form";

$mailheader = "From: $email \r\n";


if ($_POST){
if($email != "" && $message != "" && $name != ""){

mail($recipient, $subject, $formcontent, $mailheader) or die("Error!");

echo "Thank You! We shall be in contact shortly!";
echo "<script>";
echo "var clicky_custom = {};";
echo " clicky_custom.goal = { name: 'Submitted Contact' };";
echo "</script>";

}else{
echo "Please fill in all fields on the form";
}
}

?>



<form name="contactform" method="post" action="">
<table width="">
<tr>
 <td valign="top">
  <label for="name">Name *</label>
 </td>
 <td valign="top">
  <input  type="text" name="name" maxlength="50" size="30">
 </td>
</tr>

<tr>
 <td valign="top">
  <label for="email">Email Address *</label>
 </td>
 <td valign="top">
  <input  type="text" name="email" maxlength="80" size="30">
 </td>
</tr>
<tr><td width=""><label for="comments2">Comments *</label></td>
<td width="">
  <textarea  name="comments2" maxlength="1000" cols="24" rows="3"></textarea>
  </td>
</tr>
<tr>
 <td colspan="2">
   <input type="submit" value="Submit" style="width: 100px; height: 30px;">
 </td>
</tr>
</table>
</form>
</div>

</div>
</div>
<?php include "footer.php" ?>
</div>
</body>
</html>
